
module.exports = function(menu) {
    require('orchid-js')();
    const accueilPage = require('../pages/accueilPage')

    Describe('Accéder au menu ' + menu, () => {
        const accueil = new accueilPage(menu);
        It('Vérifier l\'existence du burger menu', () => {
            expect(accueil.burgerMenuExist()).toBe(true);
        })();

        It('Cliquer sur le menu burger', () => {
            accueil.clickOnBurgerMenu();
            expect(accueil.menuDeroulantVisible()).toBe(true);
        })();
        It('Vérifier l\'existence de l\'option ' + menu, () => {
            expect(accueil.optionMenuDeroulantExist()).toEqual(menu)
        })();

        It('Cliquer sur l\'option ' + menu, () => {
            accueil.clickOptionMenuDeroulant();
        })();

    })();
}
