
module.exports = function(dossier) {
    require('orchid-js')();
    const dossierPage = require('../pages/dossierPage');
    const accueilPage = require('../pages/accueilPage')

    Describe('Choisir un dossier', () => {
        const dossiers = new dossierPage(dossier);
        const accueil = new accueilPage()

        It('choisir un dossier', () => {
            dossiers.getDossier();
            expect(accueil.getHeaderButtonText()).toContain(dossier)
        })();

    })();
}
