
module.exports = function() {
    require('orchid-js')();
    const facturePage = require('../pages/facturePage')

    Describe('SC210 - Saisie du journal', () => {
        const facture = new facturePage();

        It('Verifier le titre de la page', () => {
            expect(facture.getTitle()).toContain("achats")
        })();

        It('Faire une tabulation sur le champs Journal', () => {
            facture.doTabulation("journal");
            expect(facture.checkModalErreurSaisie()).toBe(true);
        })();

        It('Cliquer sur le boutton Ok et vérifier le champs journal rouge', () => {
            expect(facture.checkModalButtonOk()).toBe(true);
            facture.clickModalButtonOk();
            expect(facture.checkChampsJournalRouge()).toBe(true);
        })();
        It('Entrer et sortir du champs journal rouge', () => {
            facture.clickChamps("journal");
            facture.clickTitle();
        })();
        //BIS
        It('Cliquer sur le boutton Ok et vérifier le champs journal rouge')();

        It('Cliquer sur la liste deroulante du journal', () => {
            facture.clickListeJournaux();
            expect(facture.checkChampsJournalRouge()).toBe(true);
            expect(facture.resultsModalListe.count()).toBeGreaterThan(1);
        })();
        It('Selectionner un journal autorise', () => {
            facture.libelleJournalSelected.getText().then((libelle) =>{
                facture.selectJournal();
                expect(element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]')).getAttribute('value')).toContain(libelle);
            })
        })();

        It('Modifier le champs Journal avec valeur complétable', () => {
            facture.modifierJournal("A");
            expect(element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]')).getAttribute('value')).toContain("A");
            expect(facture.listeFiligrane.isDisplayed()).toEqual(true);
            expect(facture.elementListeFiligrane.count()).toBeLessThanOrEqual(10);
            expect(facture.codeLigneElementListeFiligrane.getText()).toContain("A");
        })();

        It('Modifier le champs Journal avec valeur non complétable', () => {
            facture.modifierJournal("ZZ");
            expect(element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]')).getAttribute('value')).toContain("ZZ");
            expect(facture.listeFiligrane.isPresent()).toBe(false);
        })();
        It('Faire une tabulation sur le champs Journal')();
        It('Cliquer sur le boutton Ok et vérifier le champs journal rouge')();
        It('Tenter d\'ajouter un caractère supplémentaire au journal', () => {
            let journal = element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]'));
            journal.getAttribute('value').then((text) => {
                journal.sendKeys("a");
                expect(journal.getAttribute('value')).toEqual(text);
            });
        })();
        It('Cliquer sur la liste deroulante du journal')();
        It('Selectionner un journal autorise')();
        It('Cliquer sur la liste deroulante du fournisseur', () => {
            facture.clickListeFournisseur();
            expect(facture.resultsModalListe.count()).toBeGreaterThan(1);
        })();
        It('Selectionner un fournisseur autorise', () => {
            facture.selectFournisseur();
        })()
        It('Remplir le montant TTC', () => {
            expect(facture.champsMontantTTC.isDisplayed()).toBe(true);
            facture.champsMontantTTC.sendKeys("42");
        })()
        It('Ajouter une nature d\'achat', () => {
            facture.ajouterNature();
            facture.clickListeNature();
            facture.selectNature();
            browser.sleep(4000);
        })()
        It('Cliquer sur la calculatrice', () => {
            facture.cliquerCalculatrice();
        })()
        It('Enregistrer la saisie', () => {
            facture.enregistrer();
        })()

        It('Saisir le premier caractère du journal précédent', ()=>{
            let journal = element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]'));
            journal.getAttribute('value').then((text) => {
                facture.modifierJournal(text[0]);
                facture.doTabulation("journal");
            });
        })()
        It('Cliquer sur la liste deroulante du journal fin', ()=>{
            let journal = element(by.xpath('//*[@id="journal"]/div/div/input[not(@disabled)]'));
            journal.getAttribute('value').then((text) => {
                facture.clickListeJournaux();
                expect(facture.firstResultsModalListe.getText().then((code) =>{
                    return text.includes(code)
                })).toBe(true);
            })
        })();
        It('Selectionner un journal autorise')();
        It('Saisir le premier caractère du journal précédent')();
        It('Cliquer sur la liste deroulante du journal fin')();
    })();
}

