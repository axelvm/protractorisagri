
module.exports = function(typeEcriture) {
    require('orchid-js')();
    const saisiePage = require('../pages/saisiePage')

    Describe("Nouvelle écriture de " + typeEcriture, () => {
        const saisie = new saisiePage(typeEcriture);
        It('Vérifier l\'arrivée sur la page de saisie des écritures', () => {
            expect(saisie.getTitle()).toBe("Saisie des écritures");
        })();

        It('Aller sur l\'onglet ' + typeEcriture, () => {
            expect(saisie.ongletExist()).toBe(true);
            saisie.selectOnglet();
        })();

        It('Ajouter une nouvelle Ecriture', () => {
            saisie.addEcriture();
        })();
    })();
}
