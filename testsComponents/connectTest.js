
module.exports = function() {
    require('orchid-js')();
    const connexionPage = require('../pages/connexionPage');
    const dossierPage = require('../pages/dossierPage');


    Describe('connect to isacompta', () => {
        const connexion = new connexionPage();
        const dossiers = new dossierPage();

        It('accéder à Isacompta', () => {
            expect(browser.getTitle()).toEqual('Web Compta');
        })();
        It('Remplir les champs', () => {

            connexion.setCodeClient("jabba");
            connexion.setDomain("qualif");
            connexion.setUtilisateur("Compt");
            connexion.setPassword("Sopra123");
            connexion.submit();
            expect(dossiers.getTitlePage()).toEqual('Sélectionner un dossier');
        })();
    })();
}
