
let dossierPage = function(dossier) {
    this.titleSelectionDossier = element(by.id('span_titre_selection_entr_1'));
    this.itemDossier = element(by.xpath('//*[@id="span_1"][contains(.,"'+ dossier +'")]'))


    this.getTitlePage = function() {
        return this.titleSelectionDossier.getText();
    }

    this.getDossier = function(){
        browser.executeScript("arguments[0].scrollIntoView();", this.itemDossier);
        this.itemDossier.click();
    }
};
module.exports = dossierPage;
