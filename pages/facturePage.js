
let facturePage = function() {
    this.pageTitle = element(by.xpath('//div[@id="div_titre_1"]/span'));
    this.modal = element(by.xpath('//*[@id="modal"]'));
    this.modalErreurSaisie = element(by.xpath('//*[@id="modal"]/div/div/h5/strong[text()="Erreur"]/../../../div[@class="modal-body"]/div[contains(.,"Saisie du journal obligatoire")]/../../div[contains(.,"Ok")]'));
    this.modalButtonOk = element(by.xpath('//button[@class="button primary button-blue"][contains(.,"Ok")]'));
    this.modalButtonSelect = element(by.xpath('//*[@id="button_bouton_bleu_1"][not(@disabled)][contains(text(),"Sélectionner")]'));
    this.champsJournalRouge = element(by.xpath('//*[@id="journal"]/div/div/input[contains(@class,"recherche-simple-saisie")][contains(@class,"ng-touched")][contains(@class,"ng-valid")][@maxlength="2"]'));
    this.listeJournaux = element(by.xpath('//*[@id="journal"]/*/*/*[3]'));
    this.modalListeJournaux = element(by.xpath('//*[not(contains(@style,"none"))]/div/div[@class="entete-modale"]/span/strong[contains(.,"Liste des journaux")]'));
    this.modalListeFournisseurs = element(by.xpath('//*[not(contains(@style,"none"))]/div/div[@class="entete-modale"]/span/strong[contains(.,"Liste des Fournisseurs")]'))
    this.modalListeNatures = element(by.xpath('//*[not(contains(@style,"none"))]/div/div[@class="entete-modale"]/span/strong[contains(.,"Liste des natures")]'))
    this.resultsModalListe = element.all(by.xpath('//div[@id="div_recherche_avancee_re_1"]/div[contains(@class,"recherche-avancee-resultat")]'));
    this.firstResultsModalListe = element(by.xpath('//div[@id="div_recherche_avancee_re_1"]/div[contains(@class,"recherche-avancee-resultat")][1]/*[1]'))
    this.codeJournalSelected = element(by.xpath('//*[@class="recherche-avancee-resultat est-selectionne"]/div[last()-1]'));
    this.libelleJournalSelected = element(by.xpath('//*[@class="recherche-avancee-resultat est-selectionne"]/div[last()]'));
    this.listeFiligrane = element(by.id("div_resultats_recherche__1"));
    this.elementListeFiligrane = element.all(by.xpath('//*[@id="div_resultats_recherche__1"]/*'));
    this.codeLigneElementListeFiligrane = element(by.xpath('//*[@id="div_resultats_recherche__1"]/*[1]/*'));
    this.listeDeroulanteFournisseur = element(by.xpath('//*[@id="compte-contrepartie"]/*/*/*[3]'));
    this.listeDeroulanteNature = element(by.xpath('//*[@id="compte"]/*/*/*[3]'))
    this.champsMontantTTC = element(by.id("montant-credit"));
    this.boutonAjout = element(by.css(".ajout"));
    this.boutonCalculatrice = element(by.css(".calcul-automatique"));
    this.boutonEnregistrer = element(by.xpath("//*[@id='enregistrer'][contains(@class, 'co-bouton-sous-menu')]"));
    this.boutonEnregistrerOui = element(by.buttonText("Oui"));

    this.getTitle = function() {
        return this.pageTitle.getText();
    };
    this.getCodeJournalSelected = function() {
        return this.codeJournalSelected.getText();
    };
    this.getLibelleJournalSelected = function() {
        return this.libelleJournalSelected.getText();
    };

    this.clickTitle = function(){
      this.pageTitle.click();
    }
    this.checkModalErreurSaisie = function(){
        return !!this.modalErreurSaisie;
    };

    this.checkModalButtonOk = function(){
        return !!this.modalButtonOk;
    };

    this.clickModalButtonOk = function(){
        this.modalButtonOk.click();
    };

    this.checkChampsJournalRouge = function(){
      return !!this.champsJournalRouge;
    }

    this.clickListeJournaux = function(){
      this.listeJournaux.click();
      expect(!!this.modalListeJournaux).toBe(true);
    }
    /*
    * La valeur de champs doit être comprise dans ces possibilités :
    * 'journal'
    * 'compte-contrepartie'
    * ...
    */
    this.doTabulation = function(champs){
        let champsToSelect = element(by.xpath('//*[@id="' + champs +'"]/div/div/input[not(@disable)]'));
        champsToSelect.sendKeys(protractor.Key.TAB);
    };
    this.clickChamps = function(champs){
      let champsToSelect = element(by.xpath('//*[@id="' + champs +'"]/div/div/input[not(@disable)]'));
      champsToSelect.click();
    }

    this.selectJournal = function(){

        this.modalButtonSelect.click();
    }
    this.modifierJournal = function(value){
        element(by.xpath('//*[@id="journal"]/div/div/input[not(@disable)]')).sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
        element(by.xpath('//*[@id="journal"]/div/div/input[not(@disable)]')).sendKeys(value);
    };
    this.clickListeFournisseur = function(){
        this.listeDeroulanteFournisseur.click();
        expect(!!this.modalListeFournisseurs).toBe(true);
    }
    this.selectFournisseur = function(){
        this.modalButtonSelect.click();
    }

    this.clickListeNature = function(){
        this.listeDeroulanteNature.click();
        expect(!!this.modalListeNatures).toBe(true);
    }
    this.selectNature = function(){
        this.modalButtonSelect.click();
    }
    this.ajouterNature = function(){
        this.boutonAjout.click();
    }
    this.cliquerCalculatrice = function(){
        this.boutonCalculatrice.click();
    }

    this.enregistrer = function(){
        this.boutonEnregistrer.click();
        this.boutonEnregistrerOui.click();
    }

};
module.exports = facturePage;
