
let accueilPage = function(menu) {
    this.headerButtonText = element(by.id('button_1'));
    this.burgerMenu = element(by.id('img_image_bouton_menu_mo_1'));
    this.burgerList = element(by.id('div_liste_element_menu_m_1'));
    this.menuOption = element(by.xpath('//span[@id="span_texte_element_menu_m_1"][contains(.,"' + menu + '")]'));

    this.getHeaderButtonText = function() {
        return this.headerButtonText.getText();
    };

    this.burgerMenuExist = function(){
        return !!this.burgerMenu;
    };
    this.clickOnBurgerMenu = function(){
        this.burgerMenu.click()
    };

    this.menuDeroulantVisible = function(){
        return !!this.burgerList;
    };

    this.optionMenuDeroulantExist = function(){
        return this.menuOption.getText();
    }

    this.clickOptionMenuDeroulant = function(){
        return this.menuOption.click()
    }

};
module.exports = accueilPage;
