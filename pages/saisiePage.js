
let saisiePage = function(onglet) {
    this.pageTitle = element(by.xpath('//div[@id="div_titre_1"]/span'));
    this.ongletToClick = element(by.xpath('//div[@id="div_onglet_1"][contains(.,"' + onglet+ '")]'));
    this.buttonAjouterEcriture = element(by.id("button_bouton_vert_1"));

    this.getTitle = function() {
        return this.pageTitle.getText();
    };

    this.ongletExist = function(){
        return !!this.ongletToClick;
    };

    this.selectOnglet = function(){
        this.ongletToClick.click();
    };

    this.addEcriture = function (){
        this.buttonAjouterEcriture.click();
    };



};
module.exports = saisiePage;
