var connexionPage = function() {
    this.codeClientElement = element(by.id('clientId'));
    this.domaineSelectButton = element(by.xpath('//*[@id="domain"]/div[1]/div/div/span/button'));
    this.domaineDropDown = element.all(by.css('.wj-listbox-item'));
    this.utilisateurElement = element(by.id('username'));
    this.passwordElement = element(by.id('password'));
    this.submitButtonElement = element(by.id('submitButton'));

    this.setCodeClient = function(client) {
        this.codeClientElement.sendKeys(client);
    }

    /*
    * User has to choose between those values for domain : qualif, integ or dev
    * By Default, Qualification environment is selected
    */
    this.setDomain = function(domain) {
        this.domaineSelectButton.click();
        if(domain === "qualif"){
            this.domaineDropDown.get(1).click();
        }else if(domain === "dev"){
            this.domaineDropDown.get(0).click();
        }else if (domain === "integ"){
            this.domaineDropDown.get(2).click();
        }else{
            this.domaineDropDown.get(1).click();
        }

    }
    this.setUtilisateur = function(name) {
        this.utilisateurElement.sendKeys(name);
    };
    this.setPassword = function(password) {
        this.passwordElement.sendKeys(password);
    };
    this.submit = function() {
        this.submitButtonElement.click();
    };

};
module.exports = connexionPage;
