require('orchid-js')();


Test('Demo',function(){
    Test('Connexion',require('../testsComponents/connectTest'))();
    Test('Choisir un dossier', require('../testsComponents/choisirDossierTest'))("SARL 1");
    Test('accéder au menu', require('../testsComponents/accesMenuTest'))("Saisie");
    Test('Saisie des écritures', require('../testsComponents/nouvelleEcritureTest'))("Achats");
    Test('SC210 - Saisie du journal', require('../testsComponents/SC210SaisieDuJournalTest'))();
})();
